extends CharacterBody3D

@export_category("stats")
@export var health: int = 5
@export var speed: float = 2.0

@export var damage: int = 1
@export var attack_delay: float = 1.0
@export var attack_range: float = 2.0

@export var points_on_death: int = 10

@onready var player: Player = get_node("/root/MainScene/Player")
@onready var timer: Timer = get_node("AttackDelay")

func _ready():
	
	timer.start(attack_delay)

func _physics_process(delta):
	
	# Get direction to player
	var direction = (player.transform.origin - transform.origin).normalized()
	
	direction.y = 0.0
	
	velocity = direction*speed
	
	if transform.origin.distance_to(player.transform.origin) > attack_range:
		move_and_slide()

func take_damage(damage_taken):
	health -= damage_taken
	
	if health <= 0:
		_die()

func _die():
	
	player.add_score(points_on_death)
	
	queue_free()

func _attack():
	player.take_damage(damage)

func _on_attack_delay_timeout():
	
	if transform.origin.distance_to(player.transform.origin) <= attack_range:
		_attack()
	
	timer.start(attack_delay)
