extends Area3D

@export var speed: float = 30.0
@export var damage: int = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	global_translate(global_transform.basis.z * speed * delta)
	
	

func destroy():
	
	queue_free()


func _on_body_entered(body):
	
	if body.has_method("take_damage"):
		body.take_damage(damage)
		destroy()
	
	pass # Replace with function body.
