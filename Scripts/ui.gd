class_name PlayerUI

extends Control

@onready var health_bar: TextureProgressBar = get_node("HealthBar")
@onready var ammo_text: Label = get_node("AmmoValue")
@onready var score_text: Label = get_node("ScoreValue")

func update_health_bar(current_hp: float, max_hp: float):
	health_bar.max_value = max_hp
	health_bar.value = current_hp

func update_ammo_text(ammo: int):
	ammo_text.text = "Ammo: " + str(ammo)

func update_score_text(score: int):
	score_text.text = "Score: " + str(score)

