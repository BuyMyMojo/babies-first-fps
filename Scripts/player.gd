class_name Player
extends CharacterBody3D

@export_category("stats")
@export var current_hp: int = 10
@export var max_hp: int = 10
@export var max_ammo: int = 15
@export var ammo: int = 15
var score: int = 0

@export_category("character movement")
@export var speed: float = 5.0
@export var jump_velocity: float = 4.5

@export_category("camera controls")
@export var min_look_angle: float = -90.0
@export var max_look_angle: float = 90.0
@export var look_sensitivity: float = 10.0

# vectors
#var current_velocity: Vector3 = Vector3.ZERO
var mouse_delta: Vector2 = Vector2.ZERO

# components
@onready var camera: Camera3D = get_node("Camera3D")
@onready var muzzle: Node = get_node("Camera3D/Muzzle")
@onready var bullet_scene: PackedScene = preload("res://Scenes/bullet.tscn")
@onready var ui: PlayerUI = get_node("/root/MainScene/CanvasLayer/UI")

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	
	# Hide and lock mouse to window
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
	# Set UI
	ui.update_health_bar(current_hp, max_hp)
	ui.update_ammo_text(ammo)
	ui.update_score_text(score)

func _physics_process(delta):
	# Reset current mvoement velocity
	velocity.x = 0.0
	velocity.z = 0.0
	
	var input = Vector2.ZERO
	
	# Handle movement inputs to Vector
	if Input.is_action_pressed("move_forward"):
		input.y -= 1
	if Input.is_action_pressed("move_backward"):
		input.y += 1
	if Input.is_action_pressed("move_left"):
		input.x -= 1
	if Input.is_action_pressed("move_right"):
		input.x += 1
	
	input = input.normalized()
	
	# Get playing orientation
	var forward: Vector3 = global_transform.basis.z
	var right: Vector3 = global_transform.basis.x
	
	var relativeDirection: Vector3 = (forward * input.y + right * input.x )
	
	# Set current velocity
	velocity.x = relativeDirection.x * speed
	velocity.z = relativeDirection.z * speed
	
	velocity.y -= gravity * delta
	
	move_and_slide()
	
	if Input.is_action_pressed("jump") and is_on_floor():
		velocity.y = jump_velocity

func _process(delta):
	
	# rotate camera vertically
	camera.rotation_degrees.x -= mouse_delta.y * look_sensitivity * delta
	
	# clamp camera x axis
	camera.rotation_degrees.x = clamp(camera.rotation_degrees.x, min_look_angle, max_look_angle)
	
	# rotate player y axis
	rotation_degrees.y -= mouse_delta.x * look_sensitivity * delta
	
	# reset looking vector
	mouse_delta = Vector2.ZERO
	
	if Input.is_action_just_pressed("shoot") and ammo > 0:
		_shoot()

func _input(event):
	#match event:
		#InputEventMouseMotion:
			#mouse_delta = event.relative
	
	if event is InputEventMouseMotion:
		mouse_delta = event.relative

func _shoot():
	
	var bullet = bullet_scene.instantiate()
	
	get_node("/root/MainScene").add_child(bullet)
	
	bullet.global_transform = muzzle.global_transform
	
	ammo -= 1
	
	ui.update_ammo_text(ammo)

func take_damage(damage):
	current_hp -= damage
	
	if current_hp <= 0:
		current_hp = 0
		
		_die()
	
	ui.update_health_bar(current_hp, max_hp)	

func _die():
	
	get_tree().reload_current_scene()
	pass

func add_score(points):
	score += points
	
	ui.update_score_text(score)

func add_health(health):
	#current_hp += health
	#
	#if current_hp > max_hp:
		#current_hp = max_hp
	
	current_hp = clamp(current_hp + health, 0, max_hp)
	
	ui.update_health_bar(current_hp, max_hp)

func add_ammo(ammo_ammount):
	ammo += ammo_ammount
	
	ui.update_ammo_text(ammo)

# Built in
#func _physics_process(delta):
	## Add the gravity.
	#if not is_on_floor():
		#velocity.y -= gravity * delta
#
	## Handle jump.
	#if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		#velocity.y = jump_velocity
#
	## Get the input direction and handle the movement/deceleration.
	## As good practice, you should replace UI actions with custom gameplay actions.
	#var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	#var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	#if direction:
		#velocity.x = direction.x * speed
		#velocity.z = direction.z * speed
	#else:
		#velocity.x = move_toward(velocity.x, 0, speed)
		#velocity.z = move_toward(velocity.z, 0, speed)
#
	#move_and_slide()
