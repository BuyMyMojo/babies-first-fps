extends Area3D

enum PickupType {
	Health,
	Ammo
}

@export_category("stats")
@export var type: PickupType = PickupType.Health
@export var ammount: int = 10

# bobbing
@onready var starting_y_positiong: float = transform.origin.y
var bob_height: float = 0.6
var bob_speed: float = 0.75
var bobbing_up: bool = true

# rotation
var rotation_speed: float = 1.0

func _process(delta):
	
	# move up or down
	transform.origin.y += (bob_speed if bobbing_up == true else -bob_speed) * delta
	
	# if at top of bob movement change directions
	if bobbing_up and transform.origin.y > starting_y_positiong + bob_height:
		bobbing_up = false
	#if at bottom of bob movement change directions
	elif !bobbing_up and transform.origin.y < starting_y_positiong:
		bobbing_up = true
	
	rotation.y += rotation_speed * delta

# Called when another body enters the area
func _on_body_entered(body):
	if body.name == "Player":
		_pickup(body)
		queue_free()
	

# Called when the player enters the pickup area
# Gives the approriate stat and then destroys self
func _pickup(player):
	
	match type:
		PickupType.Health:
			player.add_health(ammount)
		PickupType.Ammo:
			player.add_ammo(ammount)
